import { Injectable } from '@angular/core'
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userID: number

  constructor(private router: Router, private httpClient: HttpClient) { }

  login(username: string, password: string) {
    this.httpClient.put<number>(environment.baseUrl + '/api/accounts/login/' + username + '/' + password, '')
      .toPromise()
      .then((id) => {
        if (id>=0) {
          this.userID = id
          this.router.navigate(['/overview/'+this.userID])
        }
      })
  }

  signIn(username: string, password: string) {
    this.httpClient.post<number>(environment.baseUrl + '/api/accounts/neu/' + username + '/' + password, '')
      .toPromise()
      .then((id) => {
        if (id>=0) {
          this.userID = id
          this.router.navigate(['/overview/'+this.userID])
        }
      })
  }

  logout() {
    // Endpunkt logout fehlt!!!
    this.router.navigate([''])
  }
}
