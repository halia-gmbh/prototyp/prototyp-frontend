import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Router} from '@angular/router';
import {map, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DungeonService {

  constructor(private router: Router, private httpClient: HttpClient) { }

  getAllActiveDungeons(){
    return this.httpClient.get(environment.baseUrl + '/api/dungeon/allActive')
      .pipe(
        map(dungeons => {
          const dungeonList: OverviewDungeon[] = []
          Object.keys(dungeons).forEach(dungeonKey => {
            dungeonList.push(new OverviewDungeon(+dungeonKey, dungeons[dungeonKey]))
          })
          return dungeonList
        })
      )
  }

  getAllInactiveDungeons() {
    return this.httpClient.get(environment.baseUrl+'/api/dungeonMaster/inaktiveDungeons')
      .pipe(
        retry(3),
        map(dungeons => {
          const dungeonList: OverviewDungeon[] = []
          Object.keys(dungeons).forEach(dungeonKey => {
                dungeonList.push(new OverviewDungeon(+dungeonKey, dungeons[dungeonKey]))
              })
          return dungeonList
        })
      )
  }

  createNewDungeon(dungeonName: string, userID: string) {
    this.httpClient.post(`${environment.baseUrl}/api/dungeonBuilder/${dungeonName}`, '')
      .toPromise()
      .then(dungeonID => {
        this.router.navigate([`/dungeonBuilder/${userID}/${dungeonID}`])
      })
  }

  activateDungeon(dungeonID: string, userID: string) {
    this.httpClient.post(`${environment.baseUrl}/api/dungeonMaster/${userID}/start/${dungeonID}`, '')
      .toPromise()
      .then(ans => {
        if (ans === true)
          this.router.navigate([`dungeon/master/${userID}/${dungeonID}`])
      })
  }

  startDungeon(dungeonID: string, userID: string) {
    this.httpClient.get(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}`)
      .toPromise()
      .then(ans => {
        if (ans === true)
          this.openDungeon(dungeonID, userID)
        else
          this.createCharacter(dungeonID, userID)
      })
  }

  createCharacter(dungeonID: string, userID: string) {
    this.router.navigate([`/dungeon/createCharacter/${userID}/${dungeonID}`])
  }

  openDungeon(dungeonID: string, userID: string) {
    this.httpClient.post(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}/betreten`, '')
      .toPromise()
      .then(ans => {
        if(ans === true)
          this.router.navigate([`/dungeon/${userID}/${dungeonID}`])
      })
  }


}

export class OverviewDungeon {
  id: number
  name: string

  constructor(id: number, name: string) {
    this.id = id
    this.name = name
  }
}
