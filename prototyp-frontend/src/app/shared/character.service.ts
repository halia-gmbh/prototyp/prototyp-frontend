import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Character} from '../components/character-creator/character-creator.component';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private router: Router, private httpClient: HttpClient) { }

  getCharacterInfo(dungeonID: string) {
    return this.httpClient.get(`${environment.baseUrl}/api/dungeon/${dungeonID}/charakterInfo`)
  }

  createCharacter(dungeonID: string, userID: string, characterName: string, characterDetails: Character) {
    this.httpClient.post(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}/${characterName}/charakter`, characterDetails)
      .toPromise()
      .then(ans => {
        if(ans === true)
          this.httpClient.post(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}/betreten`, '')
            .toPromise()
            .then(ans => {
              if(ans === true)
                this.router.navigate([`/dungeon/${userID}/${dungeonID}`])
            })
      })
  }

  cancel(userID: string) {
    this.router.navigate(['/overview/'+userID])
  }
}
