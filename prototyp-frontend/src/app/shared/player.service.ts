import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private router: Router, private httpClient: HttpClient) { }

  getRoomInfo(dungeonID: string, userID: string) {
    return this.httpClient.get(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}/gebe-raum`)
  }

  getCharacterInfo(dungeonID: string, userID: string) {
    return this.httpClient.get(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}/charakter`)
  }

  quitDungeon(dungeonID: string, userID: string) {
    this.httpClient.post(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}/verlassen`, '')
      .toPromise()
      .then(ans => {
        if(ans===true)
          this.router.navigate([`/overview/${userID}`])
      })
  }

  deleteCharacter(dungeonID: string, userID: string) {
    this.httpClient.post(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}/abmelden`, '')
      .toPromise()
      .then(ans => {
        if(ans===true)
          this.router.navigate([`/overview/${userID}`])
      })
  }

  getDungeonMasterID(dungeonID: string) {
    return this.httpClient.get<number>(`${environment.baseUrl}/api/dungeon/${dungeonID}/gebe-dungeonmaster`)
  }

  getPlayerIDs(dungeonID: string, userID: string) {
    return this.httpClient.get<number[]>(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}/gebe-spieler-in-raum`)
  }
  changeRoomControlled(dungeonID: string, userID: string, roomID: string) {
    return this.httpClient.post(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}/raumWechselContr/${roomID}`, '')
  }
}
