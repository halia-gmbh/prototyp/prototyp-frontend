import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MasterService {

  constructor(private router: Router, private httpClient: HttpClient) { }

  getDungeonInfo(dungeonID: string) {
    return this.httpClient.get(`${environment.baseUrl}/api/dungeonMaster/dungeon/${dungeonID}`)
  }

  quitDungeon(dungeonID: string, userID: string) {
    this.httpClient.post(`${environment.baseUrl}/api/dungeonMaster/${userID}/ende/${dungeonID}`, '')
      .toPromise()
      .then(ans => {
        if(ans===true)
          this.router.navigate([`/overview/${userID}`])
      })
  }

  getPlayerIDs(dungeonID: string, roomID: string) {
    return this.httpClient.get<number[]>(`${environment.baseUrl}/api/dungeon/${dungeonID}/${roomID}/gebeSpieler`)
  }

  // ACTIONS
  changeRoom(dungeonID: string, userID: string, roomID: string) {
    return this.httpClient.post(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}/raumWechsel/${roomID}`, '')
  }

  objectInteraction(dungeonID: string, userID:string, objectName: string) {
    return this.httpClient.put(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}/objekt/${objectName}`, '')
  }

  addObjectToRoom(dungeonID: string, raum: string, object: string) {
    return this.httpClient.post(`${environment.baseUrl}/api/dungeon/${dungeonID}/${raum}/objekt-hinzufuegen`, JSON.parse(object))
  }

  deleteObjectFromRoom(dungeonID: string, raum:string, objectName: string) {
    return this.httpClient.post(`${environment.baseUrl}/api/dungeon/${dungeonID}/${raum}/objekt-loeschen/${objectName}`, '')
  }

  addNpc(dungeonID: string, raum: string, npc: string) {
    return this.httpClient.post(`${environment.baseUrl}/api/dungeon/${dungeonID}/${raum}/npc-hinzufuegen`, JSON.parse(npc))
  }

  deleteNpc(dungeonID: string, raum: string, npcName: string) {
    return this.httpClient.post(`${environment.baseUrl}/api/dungeon/${dungeonID}/${raum}/npc-loeschen/${npcName}`, '')
  }

  changePlayerAttribute(dungeonID: string, userID: string, player: string) {
    return this.httpClient.post(`${environment.baseUrl}/api/dungeon/${dungeonID}/${userID}/attribut-aendern`, JSON.parse(player))
  }
}
