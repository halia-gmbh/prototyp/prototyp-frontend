import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DungeonBuilderService {

  constructor(private httpClient: HttpClient, private router: Router) { }

  createRoom(dungeonID: string, input: string) {
    return this.httpClient.post(`${environment.baseUrl}/api/dungeonBuilder/${dungeonID}/raum`, JSON.parse(input))
      .toPromise()
      .then(ans =>  {
        return ans
      })
  }


  connectRooms(dungeonID: string, roomID1: number, roomID2: number) {
    return this.httpClient.post(`${environment.baseUrl}/api/dungeonBuilder/${dungeonID}/raumVerknuepfung/${roomID1}/${roomID2}`, '')
      .toPromise()
      .then(ans =>  {
        return ans
      })
  }

  createRace(dungeonID: string, input: string) {
    return this.httpClient.post(`${environment.baseUrl}/api/dungeonBuilder/${dungeonID}/rasse`, JSON.parse(input))
      .toPromise()
      .then(ans =>  {
        return ans
      })
  }


  createClass(dungeonID: string, input: string) {
    return this.httpClient.post(`${environment.baseUrl}/api/dungeonBuilder/${dungeonID}/klasse`, JSON.parse(input))
      .toPromise()
      .then(ans =>  {
        return ans
      })
  }


  createEquipment(dungeonID: string, input: string) {
    return this.httpClient.post(`${environment.baseUrl}/api/dungeonBuilder/${dungeonID}/equipment`, JSON.parse(input))
      .toPromise()
      .then(ans =>  {
        return ans
      })
  }

  quitDungeonBuilder(userID: string) {
    this.router.navigate(['/overview/'+userID])
  }
}
