import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {OverviewComponent} from './components/overview/overview.component';
import {DungeonBuilderComponent} from './components/dungeon-builder/dungeon-builder.component';
import {CharacterCreatorComponent} from './components/character-creator/character-creator.component';
import {DungeonPlayerComponent} from './components/dungeon-player/dungeon-player.component';
import {DungeonMasterComponent} from './components/dungeon-master/dungeon-master.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'overview/:userID', component: OverviewComponent },
  { path: 'dungeonBuilder/:userID/:dungeonID', component: DungeonBuilderComponent },
  { path: 'dungeon/createCharacter/:userID/:dungeonID', component: CharacterCreatorComponent },
  { path: 'dungeon/:userID/:dungeonID', component: DungeonPlayerComponent },
  { path: 'dungeon/master/:userID/:dungeonID', component: DungeonMasterComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
