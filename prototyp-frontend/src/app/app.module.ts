import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { ReactiveFormsModule } from '@angular/forms';
import {LoginComponent} from './components/login/login.component';
import { OverviewComponent } from './components/overview/overview.component';
import { HeaderComponent } from './components/header/header.component';
import { DungeonBuilderComponent } from './components/dungeon-builder/dungeon-builder.component';
import { CharacterCreatorComponent } from './components/character-creator/character-creator.component';
import { DungeonPlayerComponent } from './components/dungeon-player/dungeon-player.component';
import { DungeonMasterComponent } from './components/dungeon-master/dungeon-master.component';

// BOOTSTRAP
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FooterComponent } from './components/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CheckboxModule, WavesModule, ButtonsModule, InputsModule, IconsModule, CardsModule } from 'angular-bootstrap-md'


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OverviewComponent,
    HeaderComponent,
    DungeonBuilderComponent,
    CharacterCreatorComponent,
    DungeonPlayerComponent,
    DungeonMasterComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
    BrowserAnimationsModule,
    CheckboxModule, WavesModule, ButtonsModule, InputsModule, IconsModule, CardsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
