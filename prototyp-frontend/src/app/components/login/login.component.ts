import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  signInForm: FormGroup;

  constructor(private authService: AuthService, private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
    this.signInForm = this.fb.group({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  login() {
    const resultForm = this.loginForm.value;
    if (resultForm.username && resultForm.password) {
      this.authService.login(resultForm.username, resultForm.password);
    }
  }

  signIn() {
    const resultForm = this.signInForm.value;
    if (resultForm.username && resultForm.password) {
      this.authService.signIn(resultForm.username, resultForm.password);
    }
  }
}
