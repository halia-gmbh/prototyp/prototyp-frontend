import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DungeonPlayerComponent } from './dungeon-player.component';

describe('DungeonPlayerComponent', () => {
  let component: DungeonPlayerComponent;
  let fixture: ComponentFixture<DungeonPlayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DungeonPlayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DungeonPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
