import {Component, OnDestroy, OnInit} from '@angular/core';
import {PlayerService} from '../../shared/player.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as Stomp from '@stomp/stompjs'
import * as SockJS from 'sockjs-client'
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-dungeon-player',
  templateUrl: './dungeon-player.component.html',
  styleUrls: ['./dungeon-player.component.scss']
})
export class DungeonPlayerComponent implements OnInit, OnDestroy {

  roomInfo: string
  characterInfo: string
  userID: string
  dungeonID: string
  dungeonMasterID: string
  actionForm: FormGroup
  chatForm: FormGroup
  chatMessages: string[] = []
  actionMessages: string[] = []

  constructor(private fb: FormBuilder, private playerService: PlayerService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.actionForm = this.fb.group({
      input: new FormControl('', Validators.required)
    })
    this.chatForm = this.fb.group({
      receiverID: new FormControl(''),
      input: new FormControl('', Validators.required)
    })

    this.dungeonID = this.route.snapshot.paramMap.get('dungeonID')
    this.userID = this.route.snapshot.paramMap.get('userID')
    this.playerService.getDungeonMasterID(this.dungeonID)
      .subscribe(id => this.dungeonMasterID = id.toString())

    this.playerService.getRoomInfo(this.dungeonID, this.userID)
      .subscribe(room => this.roomInfo = JSON.stringify(room))
    this.playerService.getCharacterInfo(this.dungeonID, this.userID)
      .subscribe(char => this.characterInfo = JSON.stringify(char))

    this.connect()
  }

  ngOnDestroy() {
    this.disconnect()
  }

  quit() {
    this.playerService.quitDungeon(this.dungeonID, this.userID)
  }

  deleteCharacter() {
    this.playerService.deleteCharacter(this.dungeonID, this.userID)
  }

  action() {
    const input = this.actionForm.value.input

    switch (true) {
      case(input === 'watch room'):
        this.playerService.getRoomInfo(this.dungeonID, this.userID)
          .subscribe(room => this.roomInfo = JSON.stringify(room))
        this.actionForm.reset()
        break
      case(input === 'watch character'):
        this.playerService.getCharacterInfo(this.dungeonID, this.userID)
          .subscribe(char => this.characterInfo = JSON.stringify(char))
        this.actionForm.reset()
        break
      case(input.match(/change room (\d)*/) != null):
        const room = input.replace('change room ', '')
        this.playerService.changeRoomControlled(this.dungeonID, this.userID, room).toPromise()
          .then(() => this.watchCharacter())
        this.actionForm.reset()
        break
      default:
        this.actionMessages.push(`Me: ${input}`)
        this.actionForm.reset()
        this.sendAction(this.userID, this.dungeonMasterID, input)
    }
  }

  watchCharacter() {
    this.playerService.getRoomInfo(this.dungeonID, this.userID)
      .subscribe(room => this.roomInfo = JSON.stringify(room))
  }

  chat() {
    const receiverID = this.chatForm.value.receiverID
    const message = this.chatForm.value.input

    if (!receiverID || receiverID.length == 0) {
      this.chatMessages.push(`Me to all: ${message}`)
      this.chatForm.reset()
      this.playerService.getPlayerIDs(this.dungeonID, this.userID)
        .toPromise()
        .then(ids => {
          ids.forEach(id => {
            if (id != Number.parseInt(this.userID))
              this.sendSchreien(this.userID, id.toString(), message)
          })
        })
    } else {
      this.chatMessages.push(`Me to ${receiverID}: ${message}`)
      this.chatForm.reset()
      this.sendfluestern(this.userID, receiverID, message)
    }
  }

  // CHAT
  private stompClient

  connect() {
    const socket = new SockJS(`${environment.baseUrl}/register-chat`);
    this.stompClient = Stomp.over(socket);

    const _this = this;
    this.stompClient.connect({}, function (frame) {
      console.log('Connected: ' + frame);

      _this.stompClient.subscribe(`/topic/player-chat/${_this.userID}`, function (hello) {
        console.log('chatMessage')
        _this.replyChatMessage(hello.body)
      });
      _this.stompClient.subscribe(`/topic/dm-chat/${_this.userID}`, function (hello) {
        console.log('actionMessage')
        _this.replyActionMessage(hello.body)
      })
    });

  }

  disconnect() {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }
    console.log('Disconnected!');
  }

  sendfluestern(userID: string, recieverID: string, message: string) {
    console.log('flüstern')
    this.stompClient.send(
      `/app/chat/fluestern/sender/${userID}/receiver/${recieverID}`,
      {},
      message
    );
  }
  sendSchreien(userID: string, recieverID: string, message: string) {
    console.log('schreien')
    this.stompClient.send(
      `/app/chat/schreien/sender/${userID}/receiver/${recieverID}`,
      {},
      message
    );
  }


  sendAction(userID: string, recieverID: string, message: string) {
    this.stompClient.send(
      `/app/chat/player-dm/sender/${userID}/receiver/${recieverID}`,
      {},
      message
    );
  }

  replyChatMessage(message: string) {
    this.chatMessages.push(message)
  }

  replyActionMessage(message: string) {
    this.actionMessages.push(message)
  }

}
