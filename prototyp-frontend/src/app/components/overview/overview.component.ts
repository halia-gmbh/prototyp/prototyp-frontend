import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {DungeonService, OverviewDungeon} from '../../shared/dungeon.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  newDungeonForm: FormGroup
  output: string
  activeDungeons: OverviewDungeon[] = []
  inActiveDungeons: OverviewDungeon[] = []
  userID: string


  constructor(private fb: FormBuilder, private dungeonService: DungeonService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.newDungeonForm = this.fb.group({
      dungeonName: new FormControl('')
    })

    this.userID = this.route.snapshot.paramMap.get('userID')

    this.refresh()
  }

  refresh() {
    this.dungeonService.getAllActiveDungeons().subscribe(dungeonData => {
      this.activeDungeons = dungeonData
    })
    this.dungeonService.getAllInactiveDungeons().subscribe(dungeonData => {
      this.inActiveDungeons = dungeonData
    })
  }

  createNewDungeon() {
    this.dungeonService
      .createNewDungeon(this.newDungeonForm.value.dungeonName, this.userID)
  }

  startDungeon(dungeonID: number) {
    this.dungeonService.startDungeon(dungeonID.toString(), this.userID)
  }

  activateDungeon(dungeonID: number) {
    this.dungeonService.activateDungeon(dungeonID.toString(), this.userID)
  }
}


