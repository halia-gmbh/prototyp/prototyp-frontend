import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {DungeonBuilderService} from '../../shared/dungeon-builder.service';

@Component({
  selector: 'app-dungeon-builder',
  templateUrl: './dungeon-builder.component.html',
  styleUrls: ['./dungeon-builder.component.scss']
})
export class DungeonBuilderComponent implements OnInit {

  dungeonForm: FormGroup
  currentCommandInfos: string[] = []
  currentDungeonInfos: string[] = []
  dungeonID: string
  userID: string

  constructor(private fb: FormBuilder, private dungeonBuilderService: DungeonBuilderService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.dungeonForm = this.fb.group({
      command: new FormControl('', Validators.required),
      input: new FormControl('', Validators.required)
    })
    this.dungeonID = this.route.snapshot.paramMap.get('dungeonID')
    this.userID = this.route.snapshot.paramMap.get('userID')
  }

  submitDungeonCommand() {
    const command:string = this.dungeonForm.value.command
    const input: string = this.dungeonForm.value.input

    switch (true) {
      case(command === "create room"):
        this.dungeonBuilderService.createRoom(this.dungeonID, input).then(ans => {
          if(ans){
            this.dungeonForm.reset()
            this.currentCommandInfos.push(command)
            this.currentDungeonInfos.push(JSON.stringify(ans))
          }
        })
        break
      case(command === "connect rooms"):
        const rooms = input.split(';')
        this.dungeonBuilderService.connectRooms(this.dungeonID, Number.parseInt(rooms[0]), Number.parseInt(rooms[1])).then(ans => {
          if(ans === true){
            this.dungeonForm.reset()
            this.currentCommandInfos.push(command)
            this.currentDungeonInfos.push(input)
          }
        })
        break
      case(command === "create race"):
        this.dungeonBuilderService.createRace(this.dungeonID, input).then(ans => {
          if(ans === true) {
            this.dungeonForm.reset()
            this.currentCommandInfos.push(command)
            this.currentDungeonInfos.push(input)
          }
        })
        break
      case(command === "create class"):
        this.dungeonBuilderService.createClass(this.dungeonID, input).then(ans => {
          if(ans === true) {
            this.dungeonForm.reset()
            this.currentCommandInfos.push(command)
            this.currentDungeonInfos.push(input)
          }
        })
        break
      case(command == "create equipment"):
        this.dungeonBuilderService.createEquipment(this.dungeonID, input).then(ans => {
          if(ans === true) {
            this.dungeonForm.reset()
            this.currentCommandInfos.push(command)
            this.currentDungeonInfos.push(input)
          }
        })
        break
    }
  }

  quitDungeonBuilder() {
    this.dungeonBuilderService.quitDungeonBuilder(this.userID)
  }

}
