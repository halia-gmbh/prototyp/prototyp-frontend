import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MasterService} from '../../shared/master.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import * as Stomp from '@stomp/stompjs'
import * as SockJS from 'sockjs-client'
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-dungeon-master',
  templateUrl: './dungeon-master.component.html',
  styleUrls: ['./dungeon-master.component.scss']
})
export class DungeonMasterComponent implements OnInit, OnDestroy {

  actionForm: FormGroup
  playerChatForm: FormGroup
  dungeonInfo: string
  dungeonMasterID: string
  dungeonID: string
  playerMessages: string[] = []

  constructor(private fb: FormBuilder, private masterService: MasterService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.actionForm = this.fb.group({
      command: new  FormControl('', Validators.required),
      input: new FormControl('', Validators.required)
    })
    this.playerChatForm = this.fb.group({
      command: new  FormControl('', Validators.required),
      input: new FormControl('', Validators.required)
    })

    this.dungeonID = this.route.snapshot.paramMap.get('dungeonID')
    this.dungeonMasterID = this.route.snapshot.paramMap.get('userID')

    this.refreshDungeonInfo()
    this.connect()
  }

  ngOnDestroy() {
    this.disconnect()
  }

  quit() {
    this.masterService.quitDungeon(this.dungeonID, this.dungeonMasterID)
  }

  fireAction() {
    const command:string = this.actionForm.value.command
    const input: string = this.actionForm.value.input.split(';')

    switch (true) {
      case(command === "change room"):
        this.masterService.changeRoom(this.dungeonID, input[0], input[1]).toPromise()
          .then(() => this.refreshDungeonInfo())
        break
      case(command === "interact object"):
        this.masterService.objectInteraction(this.dungeonID, input[0], input[1]).toPromise()
          .then((ans) => {
            console.log(ans)
            this.refreshDungeonInfo()
          })
        break
      case(command === "add object"):
        this.masterService.addObjectToRoom(this.dungeonID, input[0], input[1]).toPromise()
          .then(() => this.refreshDungeonInfo())
        break
      case(command === "delete object"):
        this.masterService.deleteObjectFromRoom(this.dungeonID, input[0], input[1]).toPromise()
          .then(() => this.refreshDungeonInfo())
        break
      case(command === "add npc"):
        this.masterService.addNpc(this.dungeonID, input[0], input[1]).toPromise()
          .then(() => this.refreshDungeonInfo())
        break
      case(command === "delete npc"):
        this.masterService.deleteNpc(this.dungeonID, input[0], input[1]).toPromise()
          .then(() => this.refreshDungeonInfo())
        break
      case(command === "change attribute"):
        this.masterService.changePlayerAttribute(this.dungeonID, input[0], input[1]).toPromise()
          .then(() => this.refreshDungeonInfo())
        break
    }
  }

  refreshDungeonInfo() {
    this.actionForm.reset()
    this.masterService.getDungeonInfo(this.dungeonID)
      .toPromise()
      .then(dungeonInfo => this.dungeonInfo = JSON.stringify(dungeonInfo))
  }

  sendPlayerMessage() {
    const command = this.playerChatForm.value.command.split(' ')
    const message = this.playerChatForm.value.input

    if (command[0] === "room") {
      this.playerMessages.push(`Me to ${command[1]}: ${message}`)
      this.playerChatForm.reset()
      this.masterService.getPlayerIDs(this.dungeonID, command[1])
        .toPromise()
        .then(ids => {
          ids.forEach(id => {
            if (id != Number.parseInt(this.dungeonMasterID))
              this.sendRoomMessage(this.dungeonMasterID, id.toString(), message)
          })
        })
    }
    else if (command[0] === 'player') {
      this.playerMessages.push(`Me to ${command[1]}: ${message}`)
      this.playerChatForm.reset()
      this.send(this.dungeonMasterID, command[1], message)
    }
  }

  // CHAT
  private stompClient

  connect() {
    const socket = new SockJS(`${environment.baseUrl}/register-chat`);
    this.stompClient = Stomp.over(socket);

    const _this = this;
    this.stompClient.connect({}, function (frame) {
      console.log('Connected: ' + frame);

      _this.stompClient.subscribe(`/topic/dm-chat/${_this.dungeonMasterID}`, function (hello) {
        _this.replyMessage(hello.body)
      });
    });
  }

  disconnect() {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }
    console.log('Disconnected!');
  }

  send(userID: string, recieverID: string, message: string) {
    this.stompClient.send(
      `/app/chat/dm-single/sender/${userID}/receiver/${recieverID}`,
      {},
      message
    );
  }
  sendRoomMessage(userID: string, recieverID: string, message: string) {
    this.stompClient.send(
      `/app/chat/dm-room/sender/${userID}/receiver/${recieverID}`,
      {},
      message
    );
  }

  replyMessage(message: string) {
    this.playerMessages.push(message)
  }

}
