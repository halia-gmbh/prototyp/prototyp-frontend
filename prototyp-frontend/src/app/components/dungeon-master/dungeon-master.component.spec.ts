import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DungeonMasterComponent } from './dungeon-master.component';

describe('DungeonMasterComponent', () => {
  let component: DungeonMasterComponent;
  let fixture: ComponentFixture<DungeonMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DungeonMasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DungeonMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
