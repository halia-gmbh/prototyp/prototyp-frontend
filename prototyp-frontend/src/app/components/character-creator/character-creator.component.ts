import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {CharacterService} from '../../shared/character.service';

@Component({
  selector: 'app-character-creator',
  templateUrl: './character-creator.component.html',
  styleUrls: ['./character-creator.component.scss']
})
export class CharacterCreatorComponent implements OnInit {

  characterForm: FormGroup
  races: CharacterObject[] = []
  playerClasses: CharacterObject[] = []
  equipments: CharacterObject[] =[]
  dungeonID: string
  userID: string

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private characterService: CharacterService) { }

  ngOnInit(): void {
    this.dungeonID = this.route.snapshot.paramMap.get('dungeonID')
    this.userID = this.route.snapshot.paramMap.get('userID')

    this.characterForm = this.fb.group({
      name: new FormControl('', Validators.required),
      race: new FormControl('', Validators.required),
      playerClass: new FormControl('', Validators.required),
      equipment: new FormControl('', Validators.required)
    })

    this.characterService.getCharacterInfo(this.dungeonID)
      .subscribe((info: CharacterInfo) => {
        this.races = info.races
        this.playerClasses = info.playerClasses
        this.equipments = info.equipments
      })
  }


  createCharacter() {
    const formInput = this.characterForm.value

    var race;
    this.races.forEach(r => {
      if(r.name === this.characterForm.value.race)
        race = r
    })
    var playerclass;
    this.playerClasses.forEach(r => {
      if(r.name === this.characterForm.value.playerClass)
        playerclass = r
    })
    var equipment;
    this.equipments.forEach(r => {
      if(r.name === this.characterForm.value.equipment)
        equipment = r
    })
    const player = new Character()
    player.race = race
    player.playerClass = playerclass
    player.equipment = equipment
    this.characterService.createCharacter(this.dungeonID, this.userID, formInput.name, player)
  }

  cancel() {
    this.characterService.cancel(this.userID)
  }
}

export class Character {
  race: CharacterObject
  playerClass: CharacterObject
  equipment: CharacterInfo
}

export class CharacterInfo {
  races: CharacterObject[]
  playerClasses: CharacterObject[]
  equipments: CharacterObject[]
}

export class CharacterObject {
  name: string
  healthPoints: number
  attackDamage: number
}
